package com.sans.mini.service;

import com.sans.mini.entity.Dynamic;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface DynamicService {
    //按时间顺序获取动态，列表显示
    public  List<Map<String,Object>>  selectOrderByTime();
    //加载更多
    public List<Map<String,Object>> moreDynamic(int begin,int end);
    //写动态
    public int writeDynamic(int userId,String content,String time);
    //写评论
    public int writeCommentByUserIdAndDynamicId(int userId,int dynamicId,String comment,String time);
    //显示该动态所有评论，时间倒叙
    public List<Map<String,String>> selectCommentByDynamicId(int dynamicId);
}
