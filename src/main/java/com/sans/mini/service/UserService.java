package com.sans.mini.service;

import com.sans.mini.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Map;

public interface UserService {
    //微信用户授权
    public boolean login(String openid,String nickName,String avatarUrl);
    public int selectId(String openid);
    public int ifUser(String openid);
    //查询用户所有信息
    public Map<String,Object> findUserInfo(int id);
    //修改个人信息
    public int updateUserInfo(int userId,String name,String sex,String education,String school);

}
