package com.sans.mini.service;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface WrongQuestionService {
    //添加一个错题
    public void insertOne(int questionId,int userId);
    //添加错题列表
    public void insertWrongList(int [] questionIds,int userId);
    //错题列表
    public List<Map<String,Object>> myWrongQuestion(int userId);
    //错题专练
    public List<Map<String,Object>> selectByUserIdRand(int userId);
    //删除错题
    public int deleteWrongQuestion(int wrongQuestionId);
}
