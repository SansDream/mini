package com.sans.mini.service;

import com.sans.mini.entity.AnswerNotes;

import java.sql.Time;
import java.util.List;
import java.util.Map;

public interface AnswerNotesService {
    public void insertOneNote(int userId, int typeId, String rightNumber, String time);
    //交卷事务
    public Boolean hand(int userId,String[] questionIds,int typeId, String rightNumber, String time);
    public List<Map<String,Object>> selectAllNotesByUserId(int userId);
}
