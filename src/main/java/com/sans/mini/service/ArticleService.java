package com.sans.mini.service;
import com.sans.mini.entity.Article;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface ArticleService {
    //按时间顺序获取文章，列表显示
    public List<Article> selectOrderByTime();
    //加载更多，参数第几次加载更多more
    public List<Article> selectMore(int begin,int end);
    //文章详情
    public Article findById(int id);
    //收藏文章
    public int collectArticle(int articleId,int userId);
    //取消收藏
    public int deleteCollectArticle(int articleId,int userId);
    //收藏的文章列表
    public List<Map<String,Object>> MyCollectArticle(int userId);
    //添加文章
    public int addArticle(Article article);
    //是否收藏
    public int ifCollectArticle(int articleId, int userId);
}
