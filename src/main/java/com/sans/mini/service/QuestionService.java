package com.sans.mini.service;

import com.sans.mini.entity.Question;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface QuestionService {
    public List<Map<String,Object>> selectOrderByRand(int typeId);
    //收藏题目
    public void collectQuestion(int questionId,int userId);
    //列表显示收藏的题目
    public List<Map<String,String>> selectAllCollectQuestionByUserId(int userId);
    //查看收藏的题目
    public Question selectQuestionById(int id);
    //添加题目
    public int insertQuestion(Question question);
    //查询一个题目详情
    public Map<String,Object> question(int id);

}
