package com.sans.mini.service.imp;

import com.sans.mini.dao.QuestionTypeDao;
import com.sans.mini.dao.QuestionTypeItemDao;
import com.sans.mini.entity.QuestionTypeItems;
import com.sans.mini.service.QuestionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Service
public class QuestionTypeServiceImp implements QuestionTypeService {
    @Autowired
    private QuestionTypeDao questionTypeDao;
    @Autowired
    private QuestionTypeItemDao questionTypeItemDao;
    @Override
    public List<Map<String,Object>> getQuestionTypeTree() {
        List<Map<String,Object>> tree=questionTypeDao.getQuestionType();
        List<QuestionTypeItems> secondType=questionTypeItemDao.getQuestionTypeItems();
        for(Map<String,Object> type:tree){
            List<QuestionTypeItems> items=new ArrayList<>();
            for(QuestionTypeItems item:secondType){
                if (Integer.parseInt(type.get("id").toString())==item.getTypeId()){
                    items.add(item);
                }
            }
            type.put("items",items);
        }
        return tree;
    }

    @Override
    public List<List<QuestionTypeItems>> getTypeItems() {
        List<QuestionTypeItems> list= questionTypeItemDao.getQuestionTypeItems();
        List<List<QuestionTypeItems>> result=new ArrayList<>();
        int typeId=list.get(0).getTypeId();
        List<QuestionTypeItems> items=new ArrayList<>();
        for (QuestionTypeItems item:list) {
            if (item.getTypeId()==typeId){
                items.add(item);
            }else {
                typeId=item.getTypeId();
                result.add(items);
                items.clear();
                items.add(item);
            }
        }
        return result;
    }
}
