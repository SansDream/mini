package com.sans.mini.service.imp;

import com.sans.mini.dao.AnswerNotesDao;
import com.sans.mini.entity.AnswerNotes;
import com.sans.mini.service.AnswerNotesService;
import com.sans.mini.service.WrongQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.sql.Time;
import java.util.List;
import java.util.Map;

@Service
public class AnswerNotesServiceImp implements AnswerNotesService {
    @Autowired
    private AnswerNotesDao answerNotesDao;
    //交卷事务处理用到wrongQuestionService
    @Autowired
    private WrongQuestionService wrongQuestionService;
    @Override
    public void insertOneNote(int userId, int typeId, String rightNumber, String time) {
        answerNotesDao.insertOneNote(userId,typeId,rightNumber,time);
    }
    //交卷事务
    @Override
    @Transactional//
    public Boolean hand(int userId,String[] questionIds,int typeId, String rightNumber, String time){
        try {
            answerNotesDao.insertOneNote(userId,typeId,rightNumber,time);
            if (questionIds[0]!=""){
                int[] wrongQuestionIds=new int[questionIds.length];
                for (int i=0;i<questionIds.length;i++){
                    System.out.println("wrongId:"+questionIds[i]);
                    int questionId=Integer.parseInt(questionIds[i]);
                    wrongQuestionIds[i]=questionId;
                }
                wrongQuestionService.insertWrongList(wrongQuestionIds,userId);
            }

        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//就是这一句了，加上之后，如果doDbStuff2()抛了异常,                                                                                       //doDbStuff1()是会回滚的
            return false;
        }
        return true;
    }
    @Override
    public List<Map<String,Object>> selectAllNotesByUserId(int userId) {
        return answerNotesDao.selectAllNotesByUserId(userId);
    }
}
