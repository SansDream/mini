package com.sans.mini.service.imp;

import com.sans.mini.dao.ClockDao;
import com.sans.mini.entity.Clock;
import com.sans.mini.service.ClockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClockServiceImp implements ClockService {
    @Autowired
    private ClockDao clockDao;
    @Override
    public int insertClock(int userId, String time) {
        if (userId!=0&&time!=""){
            return clockDao.insertClock(userId,time);
        }
        return 0;
    }

    @Override
    public List<Clock> selectByUserId(int userId) {
        if (userId!=0){
            return clockDao.selectByUserId(userId);
        }
        return null;
    }
}
