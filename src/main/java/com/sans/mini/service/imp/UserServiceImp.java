package com.sans.mini.service.imp;

import com.sans.mini.dao.UserDao;
import com.sans.mini.entity.User;
import com.sans.mini.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UserServiceImp implements UserService {
    @Autowired
    UserDao userDao;
    @Override
    public boolean login(String openid,String nickName,String avatarUrl) {
        if (userDao.selectUserByOpenid(openid)==null){
            User newUser=new User();
            newUser.setOpenid(openid);
            newUser.setNickName(nickName);
            newUser.setAvatarUrl(avatarUrl);
            if (userDao.login(newUser)) {
                System.out.println("新用户已保存到后台");
            }
            return true;
        }else {
            System.out.println("此用户已有使用记录");
            return false;
        }
    }

    @Override
    public int selectId(String openid) {

        return userDao.selectId(openid);
    }
    @Override
    public int ifUser(String openid) {

        return userDao.ifUser(openid);
    }
    @Override
    public Map<String,Object> findUserInfo(int id) {
        if (userDao.ifUserById(id)==1){
            //用户存在
            return userDao.findUserInfo(id);
        }
        return null;
    }

    @Override
    public int updateUserInfo(int userId, String name, String sex, String education, String school) {
        return userDao.updateUserInfo(userId,name,sex,education,school);
    }

}
