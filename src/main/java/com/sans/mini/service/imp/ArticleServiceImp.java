package com.sans.mini.service.imp;

import com.sans.mini.dao.ArticleDao;
import com.sans.mini.entity.Article;
import com.sans.mini.entity.CollectArticle;
import com.sans.mini.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImp implements ArticleService {
    @Autowired
    private ArticleDao articleDao;
    @Override
    public List<Article> selectOrderByTime() {
        List<Article> list=articleDao.selectOrderByTime();
        return list;
    }
    //加载更多，参数第几次加载更多more
    public List<Article> selectMore(int begin,int end){
        if (end>begin){
            List<Article> moreList=articleDao.selectMore(begin,end);
            return moreList;
        }
        return null;
    }
    @Override
    public Article findById(int id) {
        return articleDao.findById(id);
    }

    @Override
    public int collectArticle(int articleId, int userId) {
        if (articleId!=0&&userId!=0){
            return articleDao.collectArticle(articleId,userId);
        }
        return 0;
    }
    @Override
    //是否收藏
    public int ifCollectArticle(int articleId, int userId){
        if (articleId!=0&&userId!=0){
            Map<String,Object> article=articleDao.ifCollectArticle(articleId,userId);
            if (article!=null){
                return 1;
            }else {
                System.out.println(articleDao.ifCollectArticle(articleId,userId));
            }
        }
        return 0;
    }
    @Override
    //收藏的文章列表
    public List<Map<String,Object>> MyCollectArticle(int userId){
        if (userId!=0){
            return articleDao.MyCollectArticle(userId);
        }
        return null;
    }
    @Override
    public int deleteCollectArticle(int articleId,int userId) {
        if (articleId!=0&&userId!=0){
            return articleDao.deleteCollectArticle(articleId,userId);
        }
        return 0;

    }
    @Override
    //添加文章
    public int addArticle(Article article){
        if (article.getContent()!=""&&article.getTitle()!=""&&article.getTime()!=""){
            return articleDao.addArticle(article);
        }
        return 0;
    }

}
