package com.sans.mini.service.imp;

import com.sans.mini.dao.DynamicDao;
import com.sans.mini.entity.Dynamic;
import com.sans.mini.service.DynamicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DynamicServiceImp implements DynamicService {
    @Autowired
    private DynamicDao dynamicDao;
    @Override
    public  List<Map<String,Object>>  selectOrderByTime() {
        return dynamicDao.selectOrderByTime();
    }
    //加载更多
    public List<Map<String,Object>> moreDynamic(int begin,int end){
            return dynamicDao.moreDynamic(begin,end);
    }
    @Override
    public int writeDynamic(int userId, String content, String time) {
        return dynamicDao.writeDynamic(userId,content,time);
    }

    @Override
    public int writeCommentByUserIdAndDynamicId(int userId, int dynamicId, String comment,String time) {
        if (userId!=0&&dynamicId!=0&&comment!=""&&time!=""){
            return dynamicDao.writeCommentByUserIdAndDynamicId(userId,dynamicId,comment,time);
        }
        return 0;
    }

    @Override
    public List<Map<String,String>> selectCommentByDynamicId(int dynamicId) {
        if (dynamicId!=0){
            return dynamicDao.selectCommentByDynamicId(dynamicId);
        }
        return null;
    }
}
