package com.sans.mini.service.imp;

import com.sans.mini.dao.QuestionDao;
import com.sans.mini.entity.Question;
import com.sans.mini.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class QuestionServiceImp implements QuestionService {
    @Autowired
    QuestionDao questionDao;
    @Override
    public List<Map<String,Object>> selectOrderByRand(int typeItemId) {
        List<Map<String,Object>> questions=questionDao.selectOrderByRand(typeItemId);
        for (int i=0;i<questions.size();i++){
            Map<String,Object> question=questions.get(i);
            Map<String,String> choices=questionDao.selectChoicesByQuestionId(Integer.parseInt(question.get("id").toString()));
            System.out.println(choices);
            question.put("choices",choices);

        }

        return questions;
    }

    @Override
    public void collectQuestion(int questionId, int userId) {
        if (questionId!=0&&userId!=0){
            questionDao.collectQuestion(questionId,userId);
        }
    }

    @Override
    public List<Map<String, String>> selectAllCollectQuestionByUserId(int userId) {
        if (userId!=0){
            return questionDao.selectAllCollectQuestionByUserId(userId);
        }
        return null;
    }

    @Override
    public Question selectQuestionById(int id) {
        return questionDao.selectQuestionById(id);
    }
    @Override
    //查询一个题目详情
    public Map<String,Object> question(int id){
        if (id!=0){
            return questionDao.question(id);
        }
        return null;
    }
    //添加题目
    @Override
    public int insertQuestion(Question question){
        return questionDao.insertQuestion(question);
    }
}
