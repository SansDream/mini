package com.sans.mini.service.imp;

import com.sans.mini.dao.QuestionDao;
import com.sans.mini.dao.WrongQuestionDao;
import com.sans.mini.service.WrongQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WrongQuestionServiceImp implements WrongQuestionService {
    @Autowired
    private WrongQuestionDao wrongQuestionDao;
    @Autowired
    private QuestionDao questionDao;
    @Override
    public void insertOne(int questionId, int userId) {
        if (wrongQuestionDao.selectOne(questionId,userId)==null){
            wrongQuestionDao.insertOne(questionId,userId);
        }

    }
    //添加错题列表
    public void insertWrongList(int [] questionIds,int userId){
        for (int i=0;i<questionIds.length;i++){
            System.out.println("wrongId:"+questionIds[i]);
            wrongQuestionDao.insertOne(questionIds[i],userId);
        }
    }
    @Override
    //错题列表
    public List<Map<String,Object>> myWrongQuestion(int userId){
        if (userId!=0){
            return wrongQuestionDao.myWrongQuestion(userId);
        }
        return null;
    }
    @Override
    //错题专练
    public List<Map<String,Object>> selectByUserIdRand(int userId){
        if (userId!=0){
            List<Map<String,Object>> questions=wrongQuestionDao.selectByUserIdRand(userId);
            for (int i=0;i<questions.size();i++){
                Map<String,Object> question=questions.get(i);
                Map<String,String> choices=questionDao.selectChoicesByQuestionId(Integer.parseInt(question.get("questionId").toString()));
                System.out.println(choices);
                question.put("choices",choices);

            }

            return questions;
        }
       return null;
    }
    @Override
    //删除错题
    public int deleteWrongQuestion(int wrongQuestionId){
        return wrongQuestionDao.deleteWrongQuestion(wrongQuestionId);
    }
}
