package com.sans.mini.service;

import com.sans.mini.entity.Clock;

import java.util.List;

public interface ClockService {
    public int insertClock(int userId,String time);
    public List<Clock> selectByUserId(int userId);
}
