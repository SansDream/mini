package com.sans.mini.service;

import com.sans.mini.entity.QuestionTypeItems;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface QuestionTypeService {
    public List<Map<String,Object>> getQuestionTypeTree();
    //获取typeId所含二级目录
    public List<List<QuestionTypeItems>> getTypeItems();
}
