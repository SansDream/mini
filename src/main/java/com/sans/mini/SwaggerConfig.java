package com.sans.mini;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ClassName SwaggerConfig
 * @Desciption TODO
 * @Author 研发部 zhang
 * @Date 2021/5/18 14:25
 * @Version 1.0
 **/
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api(){
        ApiInfo apiInfo = new ApiInfoBuilder()
//                .contact(
//                new Contact(
//                        //文档的主题内容
//                        "后台开发文档","http://localhost:8080","ITYan1999@163.com"
//                ))
                .title("刷题小程序的开发文档").description("系统分为用户模块、刷题模块、打卡模块、甄选文章模块和朋友圈模块")
                .version("1.1").build();
        return new Docket(DocumentationType.SWAGGER_2)
//                .enable(true)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.any())
                //配置扫描的包，如果不配置默认扫描的是启动类所在包和其子包
                .apis(RequestHandlerSelectors.basePackage("com.sans.mini.controller"))
                .paths(PathSelectors.any())//对所有路径进行扫描
                .build();

    }
}
