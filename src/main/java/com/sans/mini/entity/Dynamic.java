package com.sans.mini.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Dynamic {
    private int id;
    private int userId;
    private String content;
    private Date time;
}
