package com.sans.mini.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "题")
public class Question {
    private int id;
    @ApiModelProperty(value = "单选/多选")
    private String type;
    private int typeId;
    @ApiModelProperty(value = "题目")
    private String question;
    @ApiModelProperty(value = "选项")
    private String choice0;
    @ApiModelProperty(value = "选项")
    private String choice1;
    @ApiModelProperty(value = "选项")
    private String choice2;
    @ApiModelProperty(value = "选项")
    private String choice3;
    @ApiModelProperty(value = "选项")
    private String choice4;
    @ApiModelProperty(value = "选项")
    private String choice5;
    @ApiModelProperty(value = "答案")
    private String right_answer;
    private int choices;
    public Question(int typeId,String type, String question, String choiceA, String choiceB, String choiceC,
                    String choiceD, String choiceE, String choiceF, String right_answer,int choices){
        this.typeId=typeId;
        this.type=type;
        this.choice0=choiceA;
        this.choice1=choiceB;
        this.choice2=choiceC;
        this.choice3=choiceD;
        this.choice4=choiceE;
        this.choice5=choiceF;
        this.question=question;
        this.right_answer=right_answer;
        this.choices=choices;
    }
}
