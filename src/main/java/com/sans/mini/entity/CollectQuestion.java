package com.sans.mini.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CollectQuestion {
    private int id;
    private int userId;
    private int questionId;
}
