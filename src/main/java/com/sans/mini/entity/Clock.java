package com.sans.mini.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "打卡记录")
public class Clock {
    @ApiModelProperty(value = "id")
    private int id;
    @ApiModelProperty(value = "用户id")
    private int userId;
    @ApiModelProperty(value = "打卡时间")
    private Date time;
}
