package com.sans.mini.entity;

import lombok.Data;

@Data
public class WrongQuestion {
    private int id;
    private int userId;
    private int questionId;
}
