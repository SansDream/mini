package com.sans.mini.entity;

import lombok.Data;

import java.util.Date;

@Data
public class AnswerNotes {
    private int id;
    private int userId;
    private int typeId;
    private String rightNumber;
    private String time;
}
