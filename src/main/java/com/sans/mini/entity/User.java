package com.sans.mini.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "用户")
public class User {
    @ApiModelProperty(name = "主键",value = "主键11")
    private int id;
    @ApiModelProperty(value = "openid")
    private String openid;
    @ApiModelProperty(value = "微信昵称")
    private String nickName;
    @ApiModelProperty(value = "姓名")
    private String name;
    @ApiModelProperty(value = "性别")
    private String sex;
    @ApiModelProperty(value = "学历")
    private String education;
    @ApiModelProperty(value = "学校")
    private String school;
    @ApiModelProperty(value = "微信头像")
    private String avatarUrl;
}
