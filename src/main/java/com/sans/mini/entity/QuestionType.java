package com.sans.mini.entity;

import lombok.Data;

@Data
public class QuestionType {
    private int id;
    private String type;
}
