package com.sans.mini.entity;

import lombok.Data;

@Data
public class QuestionTypeItems {
    private int id;
    private int typeId;
    private String name;
}
