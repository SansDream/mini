package com.sans.mini.entity;

import lombok.Data;

@Data
public class CollectArticle {
    private int id;
    private int userId;
    private int articleId;
}
