package com.sans.mini.entity;

import lombok.Data;

@Data
public class Comment {
    private int id;
    private int userId;
    private int dynamicId;
    private String comment;
}
