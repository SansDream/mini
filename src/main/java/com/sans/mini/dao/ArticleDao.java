package com.sans.mini.dao;


import com.sans.mini.entity.Article;
import com.sans.mini.entity.CollectArticle;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface ArticleDao {
    //添加文章
    @Insert("INSERT INTO article(title,content,TIME) VALUE(#{title},#{content},#{time})")
    public int addArticle(Article article);

    //时间倒叙显示最新文章
    @Select("select * from article order by time DESC limit 5")
    public List<Article> selectOrderByTime();
    //加载更多，参数第几次加载更多more
    @Select("select * from article order by time DESC limit #{begin},#{end}")
    public List<Article> selectMore(@Param("begin") int begin,@Param("end") int end);
    //文章详情
    @Select("select * from article where id=#{id}")
    public Article findById(int id);
    //收藏文章
    @Insert("insert into collect_article(articleId,userId) value(#{articleId},#{userId})")
    public int collectArticle(@Param("articleId") int articleId,@Param("userId") int userId);
    //收藏的文章列表
    @Select("SELECT collect_article.`articleId`,collect_article.`userId`,article.`title` FROM collect_article JOIN article ON collect_article.`articleId`=article.`id`\n" +
            " WHERE collect_article.`userId`=#{userId}")
    public List<Map<String,Object>> MyCollectArticle(int userId);
    //是否收藏
    @Select("select * from collect_article where articleId=#{articleId} and userId=#{userId}")
    public Map<String,Object> ifCollectArticle(@Param("articleId") int articleId, @Param("userId") int userId);
    //取消收藏
    @Delete("DELETE FROM collect_article WHERE articleId=#{articleId} and userId=#{userId}")
    public int deleteCollectArticle(@Param("articleId") int articleId,@Param("userId") int userId);

}
