package com.sans.mini.dao;

import com.sans.mini.entity.QuestionTypeItems;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface QuestionTypeItemDao {
    //获取所有二级类型
    @Select("SELECT * FROM question_type_items;")
    public List<QuestionTypeItems> getQuestionTypeItems();
    //获取typeId所含二级目录
    @Select("select * from question_type_items where typeItemId=#{typeItemId}")
    public List<QuestionTypeItems> getTypeItemsByTypeId(int typeItemId);
}
