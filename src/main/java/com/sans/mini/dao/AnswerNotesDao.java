package com.sans.mini.dao;

import com.sans.mini.entity.AnswerNotes;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.sql.Time;
import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface AnswerNotesDao {
    //添加刷题记录
    @Insert("INSERT INTO answer_notes(userId,typeId,rightNumber,TIME) VALUE(#{userId},#{typeId},#{rightNumber},#{time})")
    public void insertOneNote(@Param("userId") int userId,@Param("typeId") int typeId,@Param("rightNumber") String rightNumber,@Param("time") String time);
    //查询用户刷题记录
    @Select(" SELECT answer_notes.`rightNumber`,answer_notes.`time`,question_type_items.`name` FROM answer_notes JOIN question_type_items ON\n" +
            " answer_notes.`typeId`=question_type_items.`id` WHERE userId=#{userId}")
    public List<Map<String,Object>> selectAllNotesByUserId(int userId);
}
