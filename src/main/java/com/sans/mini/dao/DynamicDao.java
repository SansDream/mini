package com.sans.mini.dao;

import com.sans.mini.entity.Dynamic;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface DynamicDao {
    @Select("SELECT dynamic.`id`,dynamic.`userId`,dynamic.`content`,dynamic.`time`,user.`avatarUrl`,user.`nickName` FROM DYNAMIC JOIN USER ON dynamic.`userId`=User.`id` ORDER BY TIME DESC limit 10")
    public List<Map<String,Object>> selectOrderByTime();
    //加载更多
    @Select("SELECT dynamic.`id`,dynamic.`userId`,dynamic.`content`,dynamic.`time`,user.`avatarUrl`,user.`nickName` FROM DYNAMIC JOIN USER ON dynamic.`userId`=User.`id` ORDER BY TIME DESC limit #{begin},#{end}")
    public List<Map<String,Object>> moreDynamic(@Param("begin") int begin,@Param("end")int end);
    //写动态
    @Insert("INSERT INTO dynamic(userId,content,TIME) VALUE(#{userId},#{content},#{time})")
    public int writeDynamic(@Param("userId") int userId,@Param("content") String content,@Param("time") String time);
    //写评论
    @Insert("INSERT INTO COMMENT(userId,dynamicId,COMMENT,time) VALUE(#{userId},#{dynamicId},#{comment},#{time})")
    public int writeCommentByUserIdAndDynamicId(@Param("userId")int userId,@Param("dynamicId")int dynamicId,@Param("comment")String comment,@Param("time") String time);
    //显示该动态所有评论，时间倒叙
    @Select("SELECT comment.`comment`,user.`nickName` FROM COMMENT JOIN USER ON user.`id`=comment.`userId` WHERE comment.`dynamicId`=#{dynamicId} ORDER BY TIME DESC")
    public List<Map<String,String>> selectCommentByDynamicId(int dynamicId);

}
