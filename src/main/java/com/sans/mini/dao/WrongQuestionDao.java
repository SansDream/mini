package com.sans.mini.dao;

import com.sans.mini.entity.WrongQuestion;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface WrongQuestionDao {
    //添加错题记录
    @Insert("INSERT INTO wrong_question(questionId,userId) VALUE(#{questionId},#{userId})")
    public void insertOne(@Param("questionId") int questionId,@Param("userId") int userId);
    //判断错题是否已存在
    @Select("SELECT * from wrong_question where questionId=#{questionId} and userId=#{userId}")
    public WrongQuestion selectOne(@Param("questionId") int questionId, @Param("userId") int userId);
    //错题专练
    @Select("SELECT wrong_question.id,wrong_question.questionId,question.`type`,question.`question`,question.`right_answer`,question.`typeItemId`\n" +
            "FROM question JOIN wrong_question  ON wrong_question.`questionId`=question.`id`\n" +
            "WHERE wrong_question.`userId`=#{userId} ORDER BY  RAND() LIMIT 5")
    public List<Map<String,Object>> selectByUserIdRand(int userId);
    //错题列表
    @Select("SELECT wrong_question.`questionId`,question.`question` FROM wrong_question JOIN question ON wrong_question.`questionId`=question.`id`\n" +
            " WHERE wrong_question.`userId`=#{userId}")
    public List<Map<String,Object>> myWrongQuestion(int userId);
    //删除错题
    @Delete("delete from wrong_question where id=#{wrongQuestionId}")
    public int deleteWrongQuestion(int wrongQuestionId);
}
