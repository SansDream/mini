package com.sans.mini.dao;

import com.sans.mini.entity.Article;
import com.sans.mini.entity.Question;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface QuestionDao {
    //添加题目
    @Insert("INSERT INTO question(TYPE,typeItemId,question,choice0,choice1,choice2,choice3,choice4,choice5,right_answer,choices)\n" +
            "VALUE(#{type},#{typeId},#{question},#{choice0},#{choice1},#{choice2},#{choice3},#{choice4},#{choice5},#{right_answer},#{choices})")
    public int insertQuestion(Question question);
    //查询一个题目详情
    @Select("SELECT * FROM question WHERE id=#{id}")
    public Map<String,Object> question(int id);
    //专项练习
    @Select("SELECT question.id,type,typeItemId,question,right_answer " +
            "FROM question JOIN question_type_items ON question_type_items.id=question.`typeItemId` WHERE typeItemId=#{typeItemId}  ORDER BY  RAND() LIMIT 5")
    List<Map<String,Object>> selectOrderByRand(int typeItemId);
    //通过ID查询所有选项
    @Select("SELECT choice0,choice1,choice2,choice3,choice4,choice5 FROM question WHERE id=#{id}")
    Map<String,String> selectChoicesByQuestionId(int id);
    //收藏题目
    @Insert("insert into collect_question(questionId,userId) value(#{questionId},#{userId})")
    public void collectQuestion(int questionId,int userId);
    //列表显示收藏的题目
    @Select("SELECT question.`id`,question.`question` \n" +
            "FROM question JOIN collect_question ON collect_question.`questionId`=question.id\n" +
            "WHERE collect_question.`userId`=#{userId}")
    public List<Map<String,String>> selectAllCollectQuestionByUserId(int userId);
    //查看收藏的题目
    @Select("select * from question where id=#{id}")
    public Question selectQuestionById(int id);
}
