package com.sans.mini.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface QuestionTypeDao {
    //获取所有一级类型
    @Select("SELECT * FROM question_type;")
    public List<Map<String,Object>> getQuestionType();

}
