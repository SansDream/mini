package com.sans.mini.dao;

import com.sans.mini.entity.Clock;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface ClockDao {
    //打卡
    @Insert("insert INTO clock(userId,time) VALUE(#{userId},#{time})")
    public int insertClock(@Param("userId") int userId,@Param("time") String time);
    //用户打卡记录查询
    @Select("select * from clock where userId=#{userId}")
    public List<Clock> selectByUserId(int userId);
}
