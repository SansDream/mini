package com.sans.mini.dao;

import com.sans.mini.entity.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.Map;

@Mapper
@Component
public interface UserDao {
    //将首次登陆的微信用户存入数据库
    @Insert("INSERT INTO USER(openid,nickName,avatarUrl) VALUE(#{openid},#{nickName},#{avatarUrl})")
    public boolean login(User user);
    //查询openID的用户是否存在
    @Select("select * from user where openid=#{openid}")
    public User selectUserByOpenid(String openid);
    @Select("SELECT EXISTS(select id from user where openid=#{openid})")
    public int ifUser(String openid);
    //用户id是否存在
    @Select("SELECT EXISTS(select id from user where id=#{userId})")
    public int ifUserById(int userId);
    @Select("select id from user where openid=#{openid}")
    public int selectId(String openid);
    //查询用户所有信息
    @Select("select id,nickName,avatarUrl,name,sex,education,school from user where id=#{id}")
    public Map<String,Object> findUserInfo(int id);
    //修改：
    //修改姓名
    //修改性别
    //修改学校
    //修改学历
    @Update("UPDATE USER SET NAME=#{name},sex=#{sex},education=#{education},school=#{school} where id=#{id}")
    public int updateUserInfo(@Param("id") int userId,@Param("name") String name,@Param("sex")String sex,@Param("education")String education,@Param("school")String school);

}
