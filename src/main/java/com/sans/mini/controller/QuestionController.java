package com.sans.mini.controller;

import com.sans.mini.entity.Question;
import com.sans.mini.entity.QuestionTypeItems;
import com.sans.mini.service.QuestionService;
import com.sans.mini.service.QuestionTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@Api(tags = {"刷题模块"})
public class QuestionController {
    @Autowired
    QuestionService questionService;
    @Autowired
    QuestionTypeService questionTypeService;
    //专项练习
    @ResponseBody
    @RequestMapping(value = "/QuestionRand",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "typeId")
    @ApiOperation(value = "专项随机练习")
    public List<Map<String,Object>> selectOrderByRand(@RequestParam("typeId")int typeId){
//        int typeId=Integer.parseInt(request.getParameter("typeId"));
        return questionService.selectOrderByRand(typeId);
    }
    //查询一个题目详情
    @ResponseBody
    @RequestMapping(value = "/question",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "id")
    @ApiOperation(value = "题详情map")
    public Map<String,Object> question(@RequestParam("id")int id){
//        int id=Integer.parseInt(request.getParameter("id"));

        return questionService.question(id);
    }
    //收藏题目
    @ResponseBody
    @RequestMapping(value = "/collectQuestion",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int",name = "questionId"),
            @ApiImplicitParam(paramType = "int",name = "userId")
    })
    @ApiOperation(value = "收藏题目")
    public void collectQuestion(@RequestParam("questionId")int questionId,
                                @RequestParam("userId")int userId){
//        int questionId= Integer.parseInt(request.getParameter("questionId"));
//        int userId=Integer.parseInt(request.getParameter("userId"));
        questionService.collectQuestion(questionId,userId);
    }
    //列表显示收藏的题目
    @ResponseBody
    @RequestMapping(value = "/selectAllCollectQuestionByUserId",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "userId")
    @ApiOperation(value = "题目收藏列表")
    public List<Map<String,String>> selectAllCollectQuestionByUserId(@RequestParam("userId")int userId){
//        int userId=Integer.parseInt(request.getParameter("userId"));
        return questionService.selectAllCollectQuestionByUserId(userId);
    }
    //查看收藏的题目
    @ResponseBody
    @RequestMapping(value = "selectQuestionById",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "id")
    @ApiOperation(value = "查询题object")
    public Question selectQuestionById(@RequestParam("userId")int id){
//        int id=Integer.parseInt(request.getParameter("id"));
        return questionService.selectQuestionById(id);

    }
    //添加题目
    @RequestMapping(value = "addQuestionHtml",method = RequestMethod.POST)
    @ApiOperation(value = "跳转添加题库web页面")
    public String addQuestionHtml(@Param("typeItemId")int typeItemId, Model model){

        model.addAttribute("typeItemId",typeItemId);
        return "addQuestion";
    }
    //添加题库
    @ResponseBody
    @RequestMapping(value = "addQuestion",method = RequestMethod.POST)
    @ApiOperation(value = "添加题库")
    public Boolean addQuestion(HttpServletRequest request){
        String question=request.getParameter("question");
        String type=request.getParameter("type");
        int typeItemId=Integer.parseInt(request.getParameter("typeItemId"));
        String choice0=request.getParameter("choice0");
        String choice1=request.getParameter("choice1");
        String choice2=request.getParameter("choice2");
        String choice3=request.getParameter("choice3");
        String choice4=request.getParameter("choice4");
        String choice5=request.getParameter("choice5");
        int choices=0;
        if (choice5!=""){
            choices=6;
        }else if (choice4!=""){
            choices=5;
        }else if (choice3!=""){
            choices=4;
        }else if (choice2!=""){
            choices=3;
        }else if (choice1!=""){
            choices=2;
        }else if (choice0!=""){
            choices=1;
        }
        String rightAnswer=request.getParameter("rightAnswer");
        System.out.println(question);
        System.out.println(type);
        System.out.println(typeItemId);
        System.out.println(choice0);
        System.out.println(choice1);
        System.out.println(choice2);
        System.out.println(choice3);
        System.out.println(choice4);
        System.out.println(choice5);
        if (choice0==""){
            choice0=null;
        }
        if (choice1==""){
            choice1=null;
        }
        if (choice2==""){
            choice2=null;
        }
        if (choice3==""){
            choice3=null;
        }
        if (choice4==""){
            choice4=null;
        }
        if (choice5==""){
            choice5=null;
        }
        System.out.println("rightAnswer:"+rightAnswer);
        Question addQuestion=new Question(typeItemId,type,question,choice0,choice1,choice2,choice3,choice4,choice5,rightAnswer,choices);

        return false;
    }
}
