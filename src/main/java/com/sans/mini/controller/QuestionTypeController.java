package com.sans.mini.controller;

import com.sans.mini.service.QuestionTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.ws.Action;
import java.util.List;
import java.util.Map;

@Controller
@Api(tags = {"题库树"})
public class QuestionTypeController {
    @Autowired
    private QuestionTypeService questionTypeService;
    //显示题目类型 树
    @RequestMapping(value = "/tree",method = RequestMethod.GET)
    @ApiOperation(value = "获取题库分类树")
    @ResponseBody
    public List<Map<String,Object>> getQuestionTypeTree() {
        return questionTypeService.getQuestionTypeTree();
    }
    @RequestMapping(value = "/questionType",method = RequestMethod.GET)
    @ApiOperation(value = "题库类树页面")
    public String questionType(Model model){
        List<Map<String,Object>> tree=questionTypeService.getQuestionTypeTree();
        model.addAttribute("tree",tree);
        return "questionType";
    }
}
