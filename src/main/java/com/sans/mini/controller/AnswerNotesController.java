package com.sans.mini.controller;

import com.sans.mini.entity.AnswerNotes;
import com.sans.mini.service.AnswerNotesService;
import com.sans.mini.service.WrongQuestionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Time;
import java.util.List;
import java.util.Map;

@Controller
@Api(tags = {"交卷"})
public class AnswerNotesController {
    @Autowired
    private AnswerNotesService answerNotesService;
    @Autowired
    private WrongQuestionService wrongQuestionService;
    @ResponseBody
    @RequestMapping(value = "/insertNote",method = RequestMethod.POST)
    @ApiOperation(value = "交卷-添加记录")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int", name = "userId"),
            @ApiImplicitParam(paramType = "int", name = "typeId")
    })
    public void insertNote(@RequestParam("userId")int userId,
                           @RequestParam("typeId")int typeId,
                           @RequestParam("rightNumber")String rightNumber,
                           @RequestParam("time")String time){
//        int userId=Integer.parseInt(id);
//        int typeId=Integer.parseInt(typeid);
//        String rightNumber=request.getParameter("rightNumber");
//        String time=request.getParameter("time");
        if (userId!=0&&typeId!=0&&rightNumber!=""&&time!=""){
            answerNotesService.insertOneNote(userId,typeId,rightNumber,time);
        }
    }
    //交卷：添加练习记录、个人错题   出错则事务回滚
    @ResponseBody
    @RequestMapping(value = "/handPaper",method = RequestMethod.POST)
    @ApiOperation(value = "交卷-错题记录")
    public boolean handPaper(HttpServletRequest request){
        int userId=Integer.parseInt(request.getParameter("userId"));
        int typeId=Integer.parseInt(request.getParameter("typeId"));
        String rightNumber=request.getParameter("rightNumber");
        String time=request.getParameter("time");
        String[] questionIds=request.getParameter("wrongId").replace("[","").replace("]","").split(",");
        return answerNotesService.hand(userId,questionIds,typeId,rightNumber,time);
    }
    @ResponseBody
    @RequestMapping(value = "/getAllNotes",method = RequestMethod.POST)
    @ApiOperation(value = "查询用户所有练习记录")
    public List<Map<String,Object>> selectAllNotesByUserId(HttpServletRequest request){
        int userId=Integer.parseInt(request.getParameter("userId"));
        return answerNotesService.selectAllNotesByUserId(userId);
    }
}
