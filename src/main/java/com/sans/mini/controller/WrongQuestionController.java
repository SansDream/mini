package com.sans.mini.controller;

import com.sans.mini.service.WrongQuestionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@Api(tags = {"错题专练"})
public class WrongQuestionController {
    @Autowired
    private WrongQuestionService wrongQuestionService;
    @ResponseBody
    @RequestMapping(value = "/insertWrongQuestion",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int",name = "userId"),
            @ApiImplicitParam(paramType = "String",name = "wrongIds")
    })
    @ApiOperation(value = "记录错题列表")
    public void insertWrongQuestion(@RequestParam("userId")int userId,
                                    @RequestParam("wrongId")String wrongId){
//        int userId=Integer.parseInt(request.getParameter("userId"));
        System.out.println("userId:"+userId);
        String[] questionIds=wrongId.replace("[","").replace("]","").split(",");
        //有错题判断
        if (questionIds[0]!=""){
            for (int i=0;i<questionIds.length;i++){
                System.out.println("wrongId:"+questionIds[i]);
                int questionId=Integer.parseInt(questionIds[i]);
                wrongQuestionService.insertOne(questionId,userId);
            }
        }

    }
    @ResponseBody
    @RequestMapping(value = "myWrongQuestion",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "userId")
    @ApiOperation(value = "错题列表")
    //错题列表
    public List<Map<String,Object>> myWrongQuestion(@RequestParam("userId")int userId){
//        int userId=Integer.parseInt(request.getParameter("userId"));
        return wrongQuestionService.myWrongQuestion(userId);
    }
    //专项练习
    @ResponseBody
    @RequestMapping(value = "/WrongQuestionRand",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "userId")
    @ApiOperation(value = "错题专练")
    public List<Map<String,Object>> selectOrderByRand(@RequestParam("userId")int userId){
//        int userId=Integer.parseInt(request.getParameter("userId"));
        return wrongQuestionService.selectByUserIdRand(userId);
    }
    //删除错题记录
    @ResponseBody
    @RequestMapping(value = "/deleteWrongQuestions",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int",name = "userId"),
            @ApiImplicitParam(paramType = "String",name = "wrongId")
    })
    @ApiOperation(value = "删除错题记录列表")
    public Boolean deleteWrongQuestion(@RequestParam("userId")int userId,
                                       @RequestParam("wrongId")String wrongId) {
//        int userId = Integer.parseInt(request.getParameter("userId"));
        String[] wrongQuestionIds = wrongId.replace("[", "").replace("]", "").split(",");
        for (int i = 0; i < wrongQuestionIds.length; i++) {
            System.out.println("wrongId:" + wrongQuestionIds[i]);
            int wrongQuestionId = Integer.parseInt(wrongQuestionIds[i]);
            if (wrongQuestionService.deleteWrongQuestion(wrongQuestionId)==1){
                if (i==wrongQuestionIds.length-1){
                    return true;
                }
            }else {
                System.out.println(i+"删除错题记录"+wrongQuestionIds[i]+"失败，服务中断");
                break;
            }
        }
        return false;
    }
}
