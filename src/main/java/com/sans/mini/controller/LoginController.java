package com.sans.mini.controller;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.sans.mini.entity.User;
import com.sans.mini.service.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.net.www.http.HttpClient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
@Controller
@Api(tags = {"用户模块"})
public class LoginController {
    @Autowired
    UserService userService;
    static final String appid = "wx9ca50a1e5b4493ad";
    static final String secret = "38caf24ef3797f7cfba4f5b5dc3701a4";
    @ApiOperation(value = "获取微信用户openID")
    static String getOpenId(String code) throws IOException {
        String openid = "";
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid="+appid
                +"&secret="+secret+"&js_code="+code+"&grant_type=client_credential";
        //使用登录凭证code获取session_key和openID
        Map<String, String> map = new HashMap<String, String>();
        map.put("appid", appid);
        map.put("js_code", code);
        URLConnection urlConnection=new URL(url).openConnection();
        HttpURLConnection connection=(HttpURLConnection) urlConnection;
        connection.setRequestMethod("GET");
        connection.connect();
        int responseCode=connection.getResponseCode();
        if (responseCode==HttpURLConnection.HTTP_OK) {
            //得到响应流
            InputStream inputStream = connection.getInputStream();
            //获取响应
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                String[] gets = line.split(",");
                String session_key = gets[0].split(":")[1].replace("\"", "").replace("\"", "");
                openid = gets[1].split(":")[1].replace("\"", "").replace("\"", "").replace("}", "");
                System.out.println(session_key + " openid: " + openid);
            }
        }
        return openid;
    }
    @ResponseBody
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "String",name = "code"),
            @ApiImplicitParam(paramType = "String",name = "nickName"),
            @ApiImplicitParam(paramType = "String",name = "avatarUrl")
    })
    @ApiOperation(value = "登录")
    public Map<String,Object> getCode(@RequestParam("code")String code,
                                      @RequestParam("nickName")String nickName,
                                      @RequestParam("avatarUrl")String avatarUrl) throws Exception {
        Map<String,Object> userInfo=null;
//        //获取用户登录传入的code
//        String code = request.getParameter("code");
//        String nickName=request.getParameter("nickName");
//        String avatarUrl=request.getParameter("avatarUrl");
        System.out.println(code+":code");
        String openid = getOpenId(code);
        if (openid!=""){
            //微信用户授权
            //将用户基本信息存入数据库
            if (nickName!=""&&avatarUrl!=""){
                userService.login(openid,nickName,avatarUrl);
            }
            //无论用户之前是否授权，现在已经完成登录并授权，所以获取用户基本信息返回给客户端
            int userId=userService.selectId(openid);
            System.out.println(userId);
            //通过openID获取用户信息
            userInfo=userService.findUserInfo(userId);
            System.out.println(userInfo);
        }else {
            System.out.println("没有openid");
        }


        return userInfo;
    }


    @ResponseBody
    @RequestMapping(value = "/findUserByCode",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "String",name = "code")
    @ApiOperation(value = "检查用户")
//    HttpServletRequest request
    public boolean ifFindUserByCode(@RequestParam("code") String code) throws IOException {
        //获取用户登录传入的code
//        request.getParameter("code");
//        String code = c
        System.out.println("code: "+code);
        //获取openID
        String openid = getOpenId(code);
        System.out.println("openid: "+openid);
        if (openid!=""){
            //通过openID查询用户
            int ifUser=userService.ifUser(openid);
            System.out.println("ifUser "+ifUser);
            if (ifUser!=0){
                return true;
            }
        }
        return false;
    }
    @ResponseBody
    @RequestMapping(value = "/findUserInfo",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "id")
    @ApiOperation(value = "用户基本信息")
    //查询用户所有信息
    public Map<String,Object> findUserInfo(@RequestParam("id")int userId){
//        int userId=Integer.parseInt(id);
        Map<String,Object> res = userService.findUserInfo(userId);
        if (res!=null){
            return res;
        }else {
           Map<String,Object> res1 =  new HashMap<>();
            res1.put("error","没有此用户，请重新登录");
            return res1;
        }
    }

    //修改个人信息
    @RequestMapping(value = "/updateUserInfo",method = RequestMethod.POST)
    @ApiOperation(value = "修改个人信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int",name = "userId",value = "用户id"),
            @ApiImplicitParam(paramType = "string",name = "name",value = "姓名"),
            @ApiImplicitParam(paramType = "string",name = "sex",value = "性别"),
            @ApiImplicitParam(paramType = "string",name = "education",value = "学历"),
            @ApiImplicitParam(paramType = "string",name = "school",value = "学校"),
    })
    @ResponseBody
    public int updateUserInfo(@RequestParam("userId")int userId,
                              @RequestParam("name")String name,
                              @RequestParam("sex")String sex,
                              @RequestParam("education")String education,
                              @RequestParam("school")String school){
//        int userid=Integer.parseInt(userId);
//        String name=request.getParameter("name");
//        String sex=request.getParameter("sex");
//        String education=request.getParameter("education");
//        String school=request.getParameter("school");
        return userService.updateUserInfo(userId,name,sex,education,school);
    }
    @RequestMapping(value = "/index",method = RequestMethod.GET)
    @ApiOperation(value = "web主界面")
    public String index(){
        return "index";
    }
}
