package com.sans.mini.controller;

import com.sans.mini.entity.Dynamic;
import com.sans.mini.service.DynamicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@Api(tags = {"朋友圈模块"})
public class DynamicController {
    @Autowired
    private DynamicService dynamicService;
    //所有动态
    @ResponseBody
    @RequestMapping(value = "/dynamic",method = RequestMethod.GET)
    @ApiOperation(value = "讨论列表")
    public  List<Map<String,Object>>  selectOrderByTime(){
        List<Map<String,Object>> dynamics=dynamicService.selectOrderByTime();
        for (int i=0;i<dynamics.size();i++){
            int dynamicId=Integer.parseInt(dynamics.get(i).get("id").toString());
            List<Map<String,String>> comments= dynamicService.selectCommentByDynamicId(dynamicId);
            for (int j=0;j<comments.size();j++){
                System.out.println(comments.get(j).get("comment")) ;
            }

            dynamics.get(i).put("comments",comments);
        }


        return dynamics;
    }
    //加载更多动态
    @ResponseBody
    @RequestMapping(value = "/moreDynamic",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "more")
    @ApiOperation(value = "加载更多动态")
    public List<Map<String,Object>> moreDynamic(@RequestParam("more")int more){
//        int more=Integer.parseInt(request.getParameter("more"));
        int begin=more*10;
        int end=(more+1)*10;
        List<Map<String,Object>> moreDynamic=dynamicService.moreDynamic(begin,end);
        if (moreDynamic!=null){
            for (int i=0;i<moreDynamic.size();i++){

                int dynamicId=Integer.parseInt(moreDynamic.get(i).get("id").toString());
                System.out.println("dynamicId:"+moreDynamic.get(i).get("id").toString());
                List<Map<String,String>> comments= dynamicService.selectCommentByDynamicId(dynamicId);
                for (int j=0;j<comments.size();j++){
                    System.out.println(comments.get(j).get("comment")) ;
                }

                moreDynamic.get(i).put("comments",comments);
            }

        }
       if (moreDynamic.size()!=0){
            return moreDynamic;
        }
        return null;
    }
    //发表动态
    @ResponseBody
    @RequestMapping(value = "/writeDynamic",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int",name = "userId"),
            @ApiImplicitParam(paramType = "String",name = "content")
    })
    @ApiOperation(value = "发布讨论")
    public int writeDynamic(@RequestParam("userId")int userId,
                            @RequestParam("content")String content) {
//        int userId=Integer.parseInt(request.getParameter("userId"));
//        String content=request.getParameter("content");
//        String time=request.getParameter("time");
//        System.out.println(time);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        System.out.println(simpleDateFormat.format(date));
        String time=simpleDateFormat.format(date);

//        System.out.println(simpleDateFormat.format(new Date()));
        if (dynamicService.writeDynamic(userId,content,time)!=0){
            return 1;
        }
        return 0;
    }
   
    //写评论
    @ResponseBody
    @RequestMapping(value = "/writeComment",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int",name = "userId"),
            @ApiImplicitParam(paramType = "int",name = "dynamicId"),
            @ApiImplicitParam(paramType = "String",name = "comment"),
            @ApiImplicitParam(paramType = "String",name = "time"),
    })
    @ApiOperation(value = "进行评论")
    public int writeCommentByUserIdAndDynamicId(@RequestParam("userId")int userId,
                                                @RequestParam("dynamicId")int dynamicId,
                                                @RequestParam("comment")String comment,
                                                @RequestParam("time")String time){
//        int userId= Integer.parseInt(request.getParameter("userId"));
//        int dynamicId=Integer.parseInt(request.getParameter("dynamicId"));
//        String comment=request.getParameter("comment");
//        String time=request.getParameter("time");
        System.out.println("userId:"+userId+";dynamicId:"+dynamicId+";comment:"+comment+";time:"+time);
        return dynamicService.writeCommentByUserIdAndDynamicId(userId,dynamicId,comment,time);
    }
//    //显示该动态所有评论，时间倒叙
//    @ResponseBody
//    @RequestMapping("/selectComment")
//    public List<String> selectCommentByDynamicId(HttpServletRequest request){
//        int dynamicId=Integer.parseInt(request.getParameter("dynamic"));
//        return dynamicService.selectCommentByDynamicId(dynamicId);
//    }
}
