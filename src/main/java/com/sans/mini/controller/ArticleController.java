package com.sans.mini.controller;


import com.sans.mini.entity.Article;
import com.sans.mini.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.applet.AppletContext;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
//@RestController
@Api(tags = {"甄选文章模块"})
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @ResponseBody
    @RequestMapping(value = "/article",method = RequestMethod.GET)
    @ApiOperation(value = "文章列表")
    public List<Article> selectOrderByTime(){
        return articleService.selectOrderByTime();
    }

    @ResponseBody
    @RequestMapping(value = "/moreArticle",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "more")
    @ApiOperation(value = "文章分页加载更多")
    public List<Article> moreArticle(@RequestParam("more")int more){
//        int more=Integer.parseInt(request.getParameter("more"));
        System.out.println("more:"+more);
        int begin=more*5;
        int end=(more+1)*5;
        List<Article> moreList=articleService.selectMore(begin,end);
        if (moreList.size()!=0){
            return moreList;
        }
      return null;
    }

    @ResponseBody
    //文章详情
    @RequestMapping(value = "/findArticleById",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int",name = "id"),
            @ApiImplicitParam(paramType = "int",name = "userId")
    })
    @ApiOperation(value = "文章详情byId")
    public Map<String,Object> findById(@RequestParam("id")int id,
                                       @RequestParam("userId")int userId){
//        int id=Integer.parseInt(request.getParameter("id"));
//        int userId=Integer.parseInt(request.getParameter("userId"));
        int ifCollect=articleService.ifCollectArticle(id,userId);
        Map<String,Object> res = new HashMap<>();
        Article article=articleService.findById(id);
        res.put("article",article);
        res.put("ifCollect",ifCollect);
        return res;
    }
    @ResponseBody
    @RequestMapping(value = "/collectArticle",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int",name = "articleId"),
            @ApiImplicitParam(paramType = "int",name = "userId")
    })
    @ApiOperation(value = "收藏文章")
    //收藏文章
    public int collectArticle(@RequestParam("articleId")int articleId,
                              @RequestParam("userId")int userId){
//        int articleId=Integer.parseInt(request.getParameter("articleId"));
//        int userId=Integer.parseInt(request.getParameter("userId"));
        return articleService.collectArticle(articleId,userId);
    }
    @ResponseBody
    @RequestMapping(value = "/myCollectArticle",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "userId")
    @ApiOperation(value = "我的收藏列表")
    //收藏的文章列表
    public List<Map<String,Object>> MyCollectArticle(@RequestParam("userId")int userId){
//        int userId=Integer.parseInt(request.getParameter("userId"));
        return articleService.MyCollectArticle(userId);
    }
    //取消收藏
    @ResponseBody
    @RequestMapping(value = "/deleteCollectArticle",method = RequestMethod.POST)
    @ApiOperation(value = "取消收藏")
    public int deleteCollectArticle(HttpServletRequest request){
        int articleId=Integer.parseInt(request.getParameter("articleId"));
        int userId=Integer.parseInt(request.getParameter("userId"));
        return articleService.deleteCollectArticle(articleId, userId);
    }
    @RequestMapping(value = "addArticleHtml",method = RequestMethod.GET)
    @ApiOperation(value = "跳转到添加文章web界面")
    public String addArticleHtml(){
        return "addArticle";
    }
    @ResponseBody
    @RequestMapping(value = "/addArticle",method = RequestMethod.POST)
    @ApiOperation(value = "添加文章")
    public Boolean addArticle(HttpServletRequest request){
        String title=request.getParameter("title");
        String content=request.getParameter("content");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String time=formatter.format(new Date());
        System.out.println(title);
        Article article=new Article();
        article.setTitle(title);
        article.setContent(content);
        article.setTime(time);
        if (articleService.addArticle(article)==1){
            return true;
        }
        return false;
    }
}
