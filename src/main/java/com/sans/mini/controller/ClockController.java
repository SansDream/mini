package com.sans.mini.controller;

import com.sans.mini.entity.Clock;
import com.sans.mini.service.ClockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Insert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.util.List;

//@Controller
@RestController
@Api(tags = {"打卡模块"})
public class ClockController {
    @Autowired
    private ClockService clockService;
    @RequestMapping(value = "/clock",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "int",name = "userId"),
            @ApiImplicitParam(paramType = "String",name = "time")
    })
    @ApiOperation(value = "用户打卡")
    public Boolean insertClock(@RequestParam(name = "userId")int userId,
                               @RequestParam(name = "time")String time){
//        int userId= Integer.parseInt(request.getParameter("userId"));
        LocalDate date = LocalDate.now();
        System.out.println(date);
//        String time = request.getParameter("time");
        System.out.println(time);
        if (clockService.insertClock(userId,time)==1){
            return true;
        }
        return false;
    }
    @RequestMapping(value = "/myClock",method = RequestMethod.POST)
    @ApiImplicitParam(paramType = "int",name = "userId")
    @ApiOperation(value = "用户打卡记录")
    public List<Clock> selectByUserId(@RequestParam(name = "userId")int userId){
//        int userId=Integer.parseInt(request.getParameter("userId"));
        return clockService.selectByUserId(userId);
    }
}
