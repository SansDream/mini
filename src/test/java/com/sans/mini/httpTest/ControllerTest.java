package com.sans.mini.httpTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * @ClassName ControllerTest
 * @Desciption TODO
 * @Author 研发部 zhang
 * @Date 2021/5/24 10:17
 * @Version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
//不启动服务器，启动spring程序的上下文，测试http请求
public class ControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test01(){

    }
}
